public class Task3 {
    public static void main(String[] args) {
        int count = 11;
        int num1 = 1, num2 = 1;
        for (int i = 0; i < count; i++) {
            if (i == 0 || i == 1) {
                System.out.print(1 + " ");
            } else {
                System.out.print((num1 + num2) + " ");
                int temp = num2;
                num2 += num1;
                num1 = temp;
            }
        }
        System.out.println();
        num1 = 1; num2 = 1;
        while (count > 0) {
            if (count == 11 || count == 10) {
                System.out.print(1 + " ");
            } else {
                System.out.print((num1 + num2) + " ");
                int temp = num2;
                num2 += num1;
                num1 = temp;
            }
            count --;
        }
    }
}
